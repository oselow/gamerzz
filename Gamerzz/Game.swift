//
//  Game.swift
//  Gamerzz
//
//  Created by etudiant on 19/01/2022.
//

import Foundation
import UIKit
import Kingfisher

struct Game: Codable{
    let releaseDate: String?
    let name: String?
    let description: String?
    let images: [URL]?
    let id: String
    let pegi: String?
//    let pegi: String?
}
