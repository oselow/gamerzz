//
//  globalUtils.swift
//  Gamerzz
//
//  Created by etudiant on 20/01/2022.
//

import Foundation
import UIKit

class GlobalUtils {
    public func showToast(message : String, font: UIFont, backGroundColor color: UIColor = .black, textColor: UIColor = .white, view: UIViewController, position: String = "top") {

        let toastLabel: UILabel
        if position == "top" {
            toastLabel = UILabel(frame: CGRect(x: view.view.frame.size.width/2 - 75, y: view.view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0, width: 150, height: 35))
        }else{
            toastLabel = UILabel(frame: CGRect(x: view.view.frame.size.width/2 - 75, y: view.view.frame.size.height-100, width: 150, height: 35))
        }
        toastLabel.backgroundColor = color.withAlphaComponent(0.6)
        toastLabel.textColor = textColor
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        view.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
