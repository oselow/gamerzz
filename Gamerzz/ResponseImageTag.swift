//
//  ResponseImage.swift
//  Gamerzz
//
//  Created by etudiant on 18/01/2022.
//

import Foundation

struct ResponseImageTag: Codable{
    let results: [ApiImage]?
    let error: String
}
