//
//  ResponseSingleGame.swift
//  Gamerzz
//
//  Created by etudiant on 19/01/2022.
//

import Foundation
import UIKit

struct ResponseSingleGame: Codable{
    let result: ApiGame
    let error: String
    
    enum CodingKeys: String, CodingKey {
        case result = "results"
        case error
    }
}
