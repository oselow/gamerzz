//
//  ApiDivers.swift
//  Gamerzz
//
//  Created by etudiant on 19/01/2022.
//

import Foundation
import UIKit

struct ApiGameRating: Codable{
    let id: Int
    let name: String
    let api_detail_url: String
}
