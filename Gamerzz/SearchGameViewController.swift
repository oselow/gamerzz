//
//  ViewController.swift
//  Gamerzz
//
//  Created by etudiant on 18/01/2022.
//

import UIKit
import Alamofire
import Kingfisher
import Foundation

class SearchGameViewController: UIViewController {
    
//    static let dateFormatter: DateFormatter = DateFormatter()
    @IBOutlet weak var searchBarInput: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var datasource: [Game] = [] {
        didSet {
            self.filteredDatasource = self.datasource
        }
    }
    
    var filteredDatasource: [Game] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    let apiKey = "4b36ca62e0880c9d5e82ebf7cc24be2b10be0a91"
    let dataManager = DataManager()
    var utils = GlobalUtils()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Search"
                        
        tableView.dataSource = self
        tableView.delegate = self
        
        searchBarInput.delegate = self
        
        tableView.estimatedRowHeight = 120
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(refreshFavs(_:)), for: .valueChanged)
        
    
//        dataManager.loadGames(urlFetch: "https://giantbomb.com/api/games/", withCompletion: { [weak self] games in
//            self?.datasource = games
//            self?.stopLoader(loader: loading)
//        })
        
        self.loadFavGames()
    }
    
    @objc private func refreshFavs(_ sender: Any) {
        self.loadFavGames()
        self.refreshControl.endRefreshing()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tableView.reloadData()
//        self.loadFavGames()
    }
}

extension SearchGameViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension SearchGameViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredDatasource.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let game = filteredDatasource[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCellId", for: indexPath) as? GameTableViewCell
        
        cell?.nameLabel.text = game.name
        cell?.releaseDateLabel.text = game.releaseDate ?? "Non communiqué"
        cell?.gameImageView.kf.setImage(with: game.images?.first, placeholder: nil, options: nil, completionHandler: nil)
        cell?.favImageView.isHidden = dataManager.isInFav(guidToSearch: game.id) == false
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "GameDetailVCID")
        as? GameDetailViewController
        
        vc?.gameObject = filteredDatasource[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}


extension SearchGameViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchGame(searchFiled: searchBar.text ?? "")
    }
}


//utils
extension SearchGameViewController {
    public func loadFavGames(){
        let loading = self.loader()

        let taskGroup = DispatchGroup()

        let favs = dataManager.getFavs()
        self.datasource = []
        
        var nbErrors: Int = 0
        
        favs?.forEach{ fav in
            taskGroup.enter()
            dataManager.loadGame(guidGame: fav, withCompletion: { [weak self] game in
                if let game = game {
                    self?.datasource.append(game)
                }else{
                    nbErrors += 1
                }
                taskGroup.leave()
            })
        }
        
        taskGroup.notify(queue: .main){
            self.stopLoader(loader: loading)
            if nbErrors > 0 {
                self.utils.showToast(message: "\(nbErrors) erreur", font: .systemFont(ofSize: 12.0),backGroundColor: .gray, textColor: .black, view : self, position: "bottom")
            }else if self.datasource.count == 0 {
                self.utils.showToast(message: "Aucun favoris", font: UIFont.systemFont(ofSize: 12.0),backGroundColor: .gray, textColor: .black, view : self, position: "bottom")
            }
        }
    }
    
    func loader() -> UIAlertController {
        let alert = UIAlertController(title: nil, message: "Attends Valou...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x:10, y:5, width:50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.large
        loadingIndicator.startAnimating()
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        return alert
    }
    
    func stopLoader(loader: UIAlertController){
        DispatchQueue.main.async {
            loader.dismiss(animated: true, completion: nil)
            self.searchBarInput.endEditing(true)
        }
    }
    
    func searchGame(searchFiled value: String){
        self.searchBarInput.text = ""
        self.searchBarInput.endEditing(true)

        if (value.isEmpty){
            self.loadFavGames()
        }else{
            let loading = self.loader()
            dataManager.loadGames(urlFetch: "https://giantbomb.com/api/search/?ressources=game&query=\(value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")&field_list=name,guid,deck,original_release_date,image_tags,original_game_rating", withCompletion: { [weak self] games in
                self?.filteredDatasource = games
                self?.stopLoader(loader: loading)
            })
        }
//        self.filteredDatasource = self.datasource.filter{ game in
//            searchBar.text?.isEmpty ?? false || game.name!.lowercased().contains((searchBar.text)!.lowercased())
//        }
    }
}
