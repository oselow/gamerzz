//
//  Image.swift
//  Gamerzz
//
//  Created by etudiant on 19/01/2022.
//
import Foundation
import UIKit

struct ApiImage: Codable{
    let url: String?
    
    enum CodingKeys: String, CodingKey {
        case url = "screen_url"
    }
}
