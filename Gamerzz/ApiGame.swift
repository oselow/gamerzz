//
//  Game.swift
//  Gamerzz
//
//  Created by etudiant on 18/01/2022.
//

import Foundation
import UIKit

struct ApiGame: Codable{
    let releaseDate: String?
    let name: String?
    let description: String?
    let images: [ImageTag]?
    let id: String
    let rating: [ApiGameRating]?
    
    enum CodingKeys: String, CodingKey {
        case releaseDate = "original_release_date"
        case images = "image_tags"
        case name
        case description = "deck"
        case id = "guid"
        case rating = "original_game_rating"
    }
}
