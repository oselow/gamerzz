//
//  Image.swift
//  Gamerzz
//
//  Created by etudiant on 18/01/2022.
//

import Foundation
import UIKit

struct ImageTag: Codable{
    let url: String?
    
    enum CodingKeys: String, CodingKey {
        case url = "api_detail_url"
    }
}
