//
//  DataFetch.swift
//  Gamerzz
//
//  Created by etudiant on 19/01/2022.
//

import Foundation
import Alamofire

class DataManager {
    
    let apiKey = "4b36ca62e0880c9d5e82ebf7cc24be2b10be0a91"
    
    public func loadGames(urlFetch url: String, withCompletion completion: @escaping ([Game]) -> Void) {
        let parameters: Parameters = ["format": "json", "api_key": self.apiKey]
        var games: [Game] = []
            AF.request(url, method: .get,
                        parameters: parameters)
                        .validate(statusCode: [200])
                        .responseDecodable(of: ResponseGame.self) {[weak self] resp in
                            switch resp.result{
                                case .success(let responseGame):
                                let taskGroup = DispatchGroup()
                                
                                responseGame.results?.forEach { game in
                                    taskGroup.enter()
                                    var pegi: String?
                                    for info in game.rating ?? [] {
                                        if info.name.lowercased().contains("pegi"){
                                            pegi = info.name
                                            break
                                        }
                                    }
                                    
                                    self?.asyncLoadImage(imageUrl: game.images?.first?.url ?? "", completion: {result in
                                        games.append(Game(releaseDate: game.releaseDate, name: game.name, description: game.description, images: result, id: game.id, pegi: pegi))
                                        taskGroup.leave()
                                    })
                                }
                                
                                taskGroup.notify(queue: .main){
                                    completion(games)
                                }
                                                                
                                case .failure(let aferror):
                                    print(aferror.localizedDescription)
                                    completion([])
                                }
                        }
        
    }
    
    public func loadGame(guidGame id: String, withCompletion completion: @escaping (Game?) -> Void) {
        let parameters: Parameters = ["format": "json", "api_key": self.apiKey]
        AF.request("https://giantbomb.com/api/game/\(id)/", method: .get,
                    parameters: parameters)
                    .validate(statusCode: [200])
                    .responseDecodable(of: ResponseSingleGame.self) {[weak self] resp in
                        switch resp.result{
                            case .success(let responseGame):
                                let game = responseGame.result
                                var pegi: String?

                                for info in game.rating ?? [] {
                                    if info.name.lowercased().contains("pegi"){
                                        pegi = info.name
                                        break
                                    }
                                }

                                self?.asyncLoadImage(imageUrl: game.images?.first?.url ?? "", completion: {result in
                                    completion(Game(releaseDate: game.releaseDate, name: game.name, description: game.description, images: result, id: game.id, pegi: pegi))
                                })
                                                            
                            case .failure(let aferror):
                                print(aferror.localizedDescription)
                                completion(nil)
                            }
                    }
    }
    
    private func asyncLoadImage(imageUrl url: String, completion: @escaping ([URL]) -> Void) {
        let parameters: Parameters = ["format": "json", "api_key": self.apiKey]
            AF.request("\(url)", method: .get,
                        parameters: parameters)
                        .validate(statusCode: [200])
                        .responseDecodable(of: ResponseImageTag.self) { resp in
                            var urls: [URL] = []

                            switch resp.result{
                                case .success(let responseImageTag):
                                    responseImageTag.results?.forEach{ result in
                                        let url = URL(string: result.url ?? "")
                                        if let url = url{
                                            urls.append(url)
                                        }
                                    }
                                
                                case .failure(let aferror):
                                    print(aferror.localizedDescription)
                            }
                            
                            if (urls.count == 0){
                                let url = URL(string: "https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/94e56c101140441.5f182a2fceeec.png")
                                if let url = url {
                                    urls.append(url)
                                }
                            }
                            
                            completion(urls)
                        }
    }
    
    public func addToFav(idOfGame value: String){
        var favs = self.getFavs() ?? []
        favs.append(value)
        self.storeUserDefaults(valueToStore: favs, indexValue: "favs")
    }
    
    public func removeFromFav(idOfGame value: String){
        var favs = self.getFavs() ?? []
        if let favIndex = favs.firstIndex(of: value){
            favs.remove(at: favIndex)
            self.storeUserDefaults(valueToStore: favs, indexValue: "favs")
        }
    }
    
    public func getFavs() -> [String]? {
        return self.getFromUserDefaults(keyToGet: "favs")
    }
    
    public func isInFav(guidToSearch value: String) -> Bool{
        if let favs = self.getFavs(), favs.contains(value){
            return true
        }
        
        return false
    }
    
    public func storeUserDefaults(valueToStore value: [String], indexValue index: String) {
        let ud = UserDefaults.standard
        ud.set(value, forKey: index)
    }

    private func getFromUserDefaults(keyToGet index: String) -> [String]?{
        let ud = UserDefaults.standard
        let stringGived = ud.object(forKey: index) as? [String]
        return stringGived
    }
}
