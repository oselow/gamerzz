//
//  Response.swift
//  Gamerzz
//
//  Created by etudiant on 18/01/2022.
//

import Foundation
import UIKit

struct ResponseGame: Codable{
    let results: [ApiGame]?
    let error: String
}
