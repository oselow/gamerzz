//
//  GameDetailViewController.swift
//  Gamerzz
//
//  Created by etudiant on 19/01/2022.
//

import UIKit
import Kingfisher

class GameDetailViewController: UIViewController {
    var gameObject: Game!
    var dataManager = DataManager()
    var utils = GlobalUtils()

    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var gameDescription: UILabel!
    @IBOutlet weak var gameImageView: UIImageView!
    @IBOutlet weak var pegiGameImageView: UIImageView!
    
    @IBOutlet weak var favButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gameName.text = self.gameObject.name ?? ""
        self.gameDescription.text = self.gameObject.description ?? ""
        
        self.gameImageView.kf.setImage(with: self.gameObject.images?.last, placeholder: nil, options: nil, completionHandler: nil)
        
        print(self.gameObject.pegi ?? "")
        
        switch self.gameObject.pegi {
        case "PEGI: 3+" :
            pegiGameImageView.image = UIImage(named: "PEGI_3")
        case "PEGI: 7+" :
            pegiGameImageView.image = UIImage(named: "PEGI_7")
        case "PEGI: 12+" :
            pegiGameImageView.image = UIImage(named: "PEGI_12")
        case "PEGI: 16+" :
            pegiGameImageView.image = UIImage(named: "PEGI_16")
        case "PEGI: 18+" :
            pegiGameImageView.image = UIImage(named: "PEGI_18")
        case nil:
            break
        default :
            pegiGameImageView.image = UIImage(named: "PEGI_PARENTAL")
        }
        
        self.favButton.layer.cornerRadius = 10
        self.updateFavButton()
    }
    
    @IBAction func didTouchFavAction(_ sender: Any) {
        let gameId = self.gameObject.id
        if dataManager.isInFav(guidToSearch: gameId){
            dataManager.removeFromFav(idOfGame: gameId)
            
            self.utils.showToast(message: "Enlevé des favoris", font: .systemFont(ofSize: 12.0), backGroundColor: .red, view : self)
        }else{
            dataManager.addToFav(idOfGame: gameId)
            
            self.utils.showToast(message: "Ajouté aux favoris", font: .systemFont(ofSize: 12.0),backGroundColor: .green, textColor: .black, view : self)
        }
        
        self.updateFavButton()
    }
    
    private func updateFavButton(){
        if dataManager.isInFav(guidToSearch: self.gameObject.id) {
            self.favButton.setTitle("💔", for: .normal)
            self.favButton.backgroundColor = .lightGray
        }else{
            self.favButton.setTitle("❤️", for: .normal)
            self.favButton.backgroundColor = .lightGray
        }
            
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
